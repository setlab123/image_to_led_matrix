
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  ** This notice applies to any and all portions of this file
  * that are not between comment pairs USER CODE BEGIN and
  * USER CODE END. Other portions of this file, whether
  * inserted by the user or by software development tools
  * are owned by their respective copyright owners.
  *
  * COPYRIGHT(c) 2019 STMicroelectronics
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of STMicroelectronics nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "stm32f1xx_hal.h"

/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Private variables ---------------------------------------------------------*/
TIM_HandleTypeDef htim1;

UART_HandleTypeDef huart3;
DMA_HandleTypeDef hdma_usart3_rx;
DMA_HandleTypeDef hdma_usart3_tx;

/* USER CODE BEGIN PV */
/* Private variables ---------------------------------------------------------*/

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_GPIO_Init(void);
static void MX_DMA_Init(void);
static void MX_USART3_UART_Init(void);
static void MX_TIM1_Init(void);

/* USER CODE BEGIN PFP */
/* Private function prototypes -----------------------------------------------*/
void clock(void);
void lat(void);
void oe(void);


typedef struct{
	uint64_t red_bin_u[3][64];
	uint64_t green_bin_u[3][64];
	uint64_t blue_bin_u[3][64];

} Serialcommand;


volatile Serialcommand command;



uint64_t red_bin[3][64]={{0xfffffffffffbffff, 0xffbfffffffffffff, 0xffffffffffffffff, 0xffffffffffffefff, 0xffffffffffffffff, 0xfffffffffffff7ff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffff7f7ffff7ff, 0xffffffffffffffff, 0xff5ffffeffffffff, 0xffffffffffffffff, 0xfffffffffeffffff, 0xffffffffffffffff, 0xfdffffffffffffff, 0xffffffdff7ffffff, 0xffffffffefffffff, 0xfffffbffffffffff, 0xffffffffffffffff, 0xfffffff7ffffffff, 0xffffffff7fffffff, 0xffffffffffffffbf, 0xffffffffffffe69f, 0xe79ffffffdffc59f, 0xe3dfe1ff8fffc11f, 0xe0d1cffff7ffc33b, 0xe0c19fffffff8301, 0xe0c1fffeffff8601, 0xc3ff7fffff8603, 0x8083fffffbffc407, 0x8087ffdffffff000, 0xc00ffffffdfff800, 0xe3ff7ffffffffc00, 0xffff7e68fffffe01, 0xffff7e79ffffffff, 0xfffffe18ffffffff, 0xfffffe38ffffffff, 0xdfffff3dffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xbffff6ffffffffff, 0xeffffffffbfdfeff, 0xfffff7ffeffbf6ff, 0xfe7ffcff3fffffff, 0xfffffc7e7ff7ffff, 0xfffffe7e7fffffff, 0xffdffe3cffffffff, 0xffbfff00fff7ffff, 0xffffff01fffbffff, 0xffffffbfffffffff, 0xf7fffffffffdffff, 0xffffffffffffffff, 0xffffffffffbfffff, 0xfffdffffff7fffff, 0xfffefffffeffffff, 0xffffffffffffffff, 0xffff9ffff7ffffff, 0xfffef7f81fffffff, 0xffffff21ffffffff, 0xfff7ffffffffffff, 0xffeffffbffffffff, 0xffffffffffffffff},
		{0xfffffffffff0ffff, 0xff81ffffff9fbfff, 0xfe003ffffe7f9fff, 0xfcfe1ffffcff8fff, 0xffffc7fff9ff8fff, 0xf3fff3fff3ff87ff, 0xe7ffffffe7ff87ff, 0xe7fffdffdfff87ff, 0xc7fffeff9fff07ff, 0xc3fffffffffc07ff, 0xc08fff7f7ff007ff, 0xc001ffffffe00fff, 0xc01c7ffeff901fff, 0xc03e3fbfff783fff, 0xc03f9ffdfeffffff, 0xc07fcffdfdffffff, 0xf1ffeffffbffffff, 0xfffff7dbf7ffffff, 0xfffffbdbefffffff, 0xfffffbd9dfffffff, 0xffffff983fffffff, 0xfffffc103fffffff, 0xfffffc007fffffff, 0xffffe0001fffef3f, 0xffbfc00007ffc61f, 0xe71f000001ffc41f, 0xe31e000000ffc01f, 0xe0100000007f8031, 0xe0000000003f8001, 0xe0000000c01f8001, 0x1f03f00f8003, 0x3f87f80f8007, 0x80007fc7fc07f000, 0xc0007fe7fc07f800, 0xc3f07feffe0ffc00, 0xeff87e28fe1ffe01, 0xfffc7e49fe3effff, 0xfffc7e087c3fffff, 0xfffe7e28fc7fffff, 0xdffe7f3dfc6fffff, 0xdffe3ffff8ffffff, 0xdffe1ffff0ffffff, 0x99fe067c01fffdff, 0x807e003c01fc38ff, 0x803e000001f810ff, 0x803e000003f800ff, 0x803e007e03f001ff, 0x801e007e03f001ff, 0x801e003807f001ff, 0x801e000007f003ff, 0xc03e000007f007ff, 0xe03e003c07f80fff, 0xf0fc001807fc1fff, 0xfffc0000071fffff, 0xfffc0000003fffff, 0xfffc0000007fffff, 0xfffe000000ffffff, 0xffff800001ffffff, 0xffff800007ffffff, 0xfffe000001ffffff, 0xfffc0000003fffff, 0xfff0007c001fffff, 0xffe00078000fffff, 0xffc000780007ffff},
		{0xfffffffffff0ffff, 0xff80ffffff803fff, 0xfe003ffffe001fff, 0xf8000ffffc000fff, 0xf00007fff8000fff, 0xf00003fff00007ff, 0xe00001ffe00007ff, 0xe00001ffc00007ff, 0xc00000ff800007ff, 0xc00000ff800007ff, 0xc000007f000007ff, 0xc000007f00000fff, 0xc01c003e00001fff, 0xc03e003e00783fff, 0xc03f803c00ffffff, 0xc07fc03c01ffffff, 0xe1ffe01c03ffffff, 0xfffff01807ffffff, 0xfffff8180fffffff, 0xfffff8181fffffff, 0xfffffc103fffffff, 0xfffffc103fffffff, 0xfffffc007fffffff, 0xffffe0000fffef3f, 0xffbf800007ffc61f, 0xe71f000001ffc41f, 0xe31e000000ffc01f, 0xe0100000007f8031, 0xe0000000003f8001, 0xe0000000401f8001, 0xf03f00f8003, 0x3f87f8078007, 0x80003fc7fc07e000, 0x80007fe7fc07f800, 0xc1e07feffc0ffc00, 0xeff87e28fc0ffe01, 0xeff87e497c1efeff, 0xfffc7e087c3fffff, 0xfffc7e08fc3ffeff, 0xdffc3f3dfc6fffff, 0xdffe3ffff87bdfff, 0xdefe1fbdf0fdfdff, 0x98fe003c00ff7cff, 0x801e001801fc38ff, 0x803e000001f810ff, 0x803e000001f000ff, 0x801e007e037001ff, 0x801e007c03f001ff, 0x801e003003f001ff, 0x801e000007f003ff, 0xc03e000007f007ff, 0xc03c003807f00fff, 0xf07c001007dc1fff, 0xfffc0000061fffff, 0xfffc0000003fffff, 0xffbc0000007fffff, 0xffde000000ffffff, 0xffff800001ffffff, 0xffff800007ffffff, 0xfffe000000ffffff, 0xfff80000003fffff, 0xfff0007c001fffff, 0xffe00078000fffff, 0xffc000780007ffff},
		};



uint64_t green_bin[3][64]={{0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xfffffe78ffffffff, 0xfffffe79fffeffff, 0xfffffe18ffffffff, 0xfffffe38ffffffff, 0xffffff3dffefffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xfffffdffbfffffff, 0xfffffc7e7fffffff, 0xfffffe7e7fffffff, 0xfffffe3cffffffff, 0xffffff01ffffffff, 0xffffff81ffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff},
		{0xfffffffffffaffff, 0xffffffffffffffff, 0xffffbfffffffffff, 0xffffffffffffefff, 0xfffff7ffffffffff, 0xfffffbffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffff7f7ffff7ff, 0xffffffffffffffff, 0xff5ffffeffffffff, 0xfffeffffffffffff, 0xfffffffffeffffff, 0xffffffffffffffff, 0xfdffffffffffffff, 0xffffffdbf7ffffff, 0xffffffffffffffff, 0xfffffbffffffffff, 0xffffffffffffffff, 0xfffffdffffffffff, 0xffffffff7fffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xeffffffffdffffff, 0xffffe1ff8ffffbdf, 0xfcf7cffff7fffff9, 0xffefdfffffffbfff, 0xfffffffefffffffd, 0x7fffff7ffffffffb, 0xfffbfffffbffbff7, 0xfff7ffdffffdfffe, 0xfffffffffdfbf3ff, 0xd9ef7ffffff7e3ff, 0xe1ff7e28ffefc1fd, 0xe1fb7e59fffe81ff, 0xe0fffe187fff00ff, 0xe07ffe38ffbe00ff, 0xc03dff1dffec01ff, 0xe03dffffff7801ff, 0xe03dffbffff001ff, 0xa01df6dbfee003ff, 0xef1fffe7fbc1c2ff, 0xff9ff7ffedc3e6ff, 0xffdffcff3dc7ffff, 0xffdffc7e7f0fffff, 0xffdffe7e7b8fffff, 0xffdffe38fb8fffff, 0xffdfff00fb8fffff, 0xffddff01fb83ffff, 0xff9dff83fb0fffff, 0xf71dffcff81fffff, 0xff1ffffff87fffff, 0xff9fffffffbfffff, 0xff9fffffffffffff, 0xffcffffffeffffff, 0xffffffffffffffff, 0xffff9ffff7ffffff, 0xfffef7ffdfffffff, 0xffffff39ffffffff, 0xfff7ffffffffffff, 0xffeffffbffffffff, 0xffffffffffffffff},
		{0xfffffffffff0ffff, 0xff81ffffffbfbfff, 0xfe003ffffe3f9fff, 0xf8700ffffcff8fff, 0xf9ff07fff9ff8fff, 0xf3ffc3fff1ff07ff, 0xe3fff3ffe7ff07ff, 0xe7fffdffcfff07ff, 0xc3fffeffbffc07ff, 0xc3ffffffbff807ff, 0xc003ff7f3fe007ff, 0xc000ffff7fc00fff, 0xc01c7ffeff801fff, 0xc03e3fbffe783fff, 0xc03f9ffdfcffffff, 0xc07fcfbff9ffffff, 0xf1ffe79dfbffffff, 0xfffff39bf7ffffff, 0xfffff9d9efffffff, 0xfffff8999fffffff, 0xfffffc103fffffff, 0xfffffc103fffffff, 0xfffffc007fffffff, 0xffffe0001fffef3f, 0xffbf800007ffc61f, 0xe71f000001ffc41f, 0xe39e000000ffc11f, 0xe0900000007f8231, 0xe0800000003f8201, 0xe0800000c01f8201, 0x800f03f00f8403, 0x803f87f8070007, 0x80807fc7fc040000, 0x80007fe7fc000000, 0xc0007feffc000000, 0xe0007e28fe000001, 0xe0007e497e0000ff, 0xe0007e087c0000ff, 0xe0007e08fc0000ff, 0xc0007f01fc0000ff, 0xc0003f83f80001ff, 0xc0001f81f00001ff, 0x80000400000001ff, 0x80000000000000ff, 0x80000000000000ff, 0x80000000000000ff, 0x8000007e000001ff, 0x8000007c000001ff, 0x80000030000001ff, 0x80000000000003ff, 0xc0000000000007ff, 0xe000000000000fff, 0xf0000000001c1fff, 0xff000000001fffff, 0xff000000003fffff, 0xff840000007fffff, 0xffc6000000ffffff, 0xffef800001ffffff, 0xffff800007ffffff, 0xfffe000001ffffff, 0xfffc0000003fffff, 0xfff0007c001fffff, 0xffe00078000fffff, 0xffc000780007ffff},
		};





uint64_t blue_bin[3][64]={{0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffbf, 0xffffffffffffe7df, 0xe7dfffffffffc59f, 0xe3dfffffffffc39f, 0xe0d1ffffffffc33f, 0xe1c1ffffffff8721, 0xe1c1ffffffff8701, 0x1c3ffffffff8e03, 0x81c7ffffffff8e0f, 0x81cffffffffc0000, 0xc08ffffffff9e000, 0xe07ffffffff3c000, 0xe0e7fe78ffe78101, 0xe037fe79ffee81ff, 0xe013fe18ffdc00ff, 0xe03bfe38fff800ff, 0xe01bff3dffe001ff, 0xe019ffffffe001ff, 0xe019ffffffe003ff, 0xe719ffffffc083ff, 0xff9dffffff83c7ff, 0xffcdffffff87efff, 0xffcdfdfffd0fffff, 0xffedfc7e7c0fffff, 0xffedfe7e7c0fffff, 0xffedff3cfc0fffff, 0xffedff01f80fffff, 0xffcdff81f80fffff, 0xffcffffff80fffff, 0xff8ffffff83fffff, 0xff0bfffff8ffffff, 0xff8bffffffffffff, 0xffc7ffffffffffff, 0xffe7ffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff},
		{0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffef3f, 0xffbfffffffffc61f, 0xe71fffffffffc41f, 0xe31fffffffffc01f, 0xe011ffffffff8031, 0xe001ffffffff8001, 0xe001ffffffff8001, 0x3ffffffff8003, 0x3ffffffff0007, 0x8007fffffffc0000, 0xc00ffffffff80000, 0xc00ffffffff00000, 0xe007fe38ffe00001, 0xe003fe79ffe000ff, 0xe003fe187fc000ff, 0xe003fe38ff8000ff, 0xe001ff3dff8001ff, 0xe001ffffff0001ff, 0xe001ffffff0001ff, 0xe001fffbfe0003ff, 0xff01fffffe03c3ff, 0xff81fffffc07efff, 0xffc1fdffbc07ffff, 0xffc1fc7e7c0fffff, 0xffc1fe7e780fffff, 0xffe1fe38f80fffff, 0xffc1ff00f80fffff, 0xffc1ff81f807ffff, 0xffc1ff83f807ffff, 0xff03fffff81fffff, 0xff03fffff87fffff, 0xff83ffffffffffff, 0xff87ffffffffffff, 0xffc7ffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff},
		{0xfffffffffff0ffff, 0xffbdffffffbfbfff, 0xffffbffffeffffff, 0xfbfffffffdffefff, 0xfffff7ffffffffff, 0xfffffbfffffff7ff, 0xefffffffffffffff, 0xffffffffffffffff, 0xdffffeffbfffc7ff, 0xffffffffffff07ff, 0xffffff7f7ffe07ff, 0xc3bffffffffe0fff, 0xc05ffffefff01fff, 0xc03effbffff87fff, 0xc03ffffffeffffff, 0xc07ffffffdffffff, 0xf1fffffffbffffff, 0xffffffdbf7ffffff, 0xffffffdfefffffff, 0xfffffbffffffffff, 0xffffffffffffffff, 0xfffffdf7bfffffff, 0xfffffc017fffffff, 0xfffff8011fffef3f, 0xffbfe1e00fffc61f, 0xe71f07fc01ffc41f, 0xe31f21ff89ffc01f, 0xe010cffff4ff8031, 0xe0009ffff83f8001, 0xe001fbfede1f8001, 0x3ff7ff7cf8003, 0x3ffbffbff0007, 0x8007ffdffffc0000, 0x800ffff7fdf80000, 0xc00f7feffdf00000, 0xe0077e28ffe00001, 0xe0037e497fc000ff, 0xe0037e187dc000ff, 0xe001fe18fd8000ff, 0xc001ff01ff8000ff, 0xe001ff83ff0001ff, 0xe001ff83ff0001ff, 0xa001f603fe0001ff, 0xe901ffc3fa01c2ff, 0xf801f7ffec0066ff, 0xf841f8ff3c007eff, 0xf801fc7e3c007fff, 0xf801fe7c78007fff, 0xf801fe30f8007dff, 0xb801ff00f800ffff, 0xf8007f01f802ffff, 0xf0803f81f800ffff, 0xf4001fc7f81ddfff, 0xff0007fff81fffff, 0xff0000fffb3fffff, 0xff84003fc07fffff, 0xffc6000000ffffff, 0xffef800001ffffff, 0xffff800007ffffff, 0xfffef0001dffffff, 0xffffff01ffbfffff, 0xfff7ffffffffffff, 0xffeffffbffffffff, 0xffdfffffffffffff},
		};







uint8_t test_0[16][12]={
		  {0,0,0,0,0,1,1,0,0,0,0,0},
		  {0,0,0,1,1,1,1,1,1,0,0,0},
		  {0,0,1,1,1,1,1,1,1,1,0,0},
		  {0,1,1,1,1,1,1,1,1,1,1,0},
		  {0,1,1,1,1,1,1,1,1,1,1,0},
		  {0,1,1,1,1,0,0,1,1,1,1,0},
		  {1,1,1,1,0,0,0,0,1,1,1,1},
		  {1,1,1,1,0,0,0,0,1,1,1,1},
		  {1,1,1,1,0,0,0,0,1,1,1,1},
		  {1,1,1,1,0,0,0,0,1,1,1,1},
		  {0,1,1,1,1,0,0,1,1,1,1,0},
		  {0,1,1,1,1,1,1,1,1,1,1,0},
		  {0,1,1,1,1,1,1,1,1,1,1,0},
		  {0,0,1,1,1,1,1,1,1,1,0,0},
		  {0,0,0,1,1,1,1,1,1,0,0,0},
		  {0,0,0,0,0,1,1,0,0,0,0,0}
};

uint8_t test_1[16][12]={
		  {0,0,0,0,0,1,1,1,0,0,0,0},
		  {0,0,0,0,1,1,1,1,0,0,0,0},
		  {0,0,0,1,1,1,1,1,0,0,0,0},
		  {0,0,1,1,1,1,1,1,0,0,0,0},
		  {0,1,1,1,0,1,1,1,0,0,0,0},
		  {0,0,0,0,0,1,1,1,0,0,0,0},
		  {0,0,0,0,0,1,1,1,0,0,0,0},
		  {0,0,0,0,0,1,1,1,0,0,0,0},
		  {0,0,0,0,0,1,1,1,0,0,0,0},
		  {0,0,0,0,0,1,1,1,0,0,0,0},
		  {0,0,0,0,0,1,1,1,0,0,0,0},
		  {0,0,0,0,0,1,1,1,0,0,0,0},
		  {0,0,0,0,0,1,1,1,0,0,0,0},
		  {0,0,0,0,0,1,1,1,0,0,0,0},
		  {0,1,1,1,1,1,1,1,1,1,1,0},
		  {0,1,1,1,1,1,1,1,1,1,1,0},
};

uint8_t test_2[16][12]={
		  {0,0,0,0,0,1,1,1,1,0,0,0},
		  {0,0,0,1,1,1,1,1,1,1,0,0},
		  {0,0,1,1,1,1,1,1,1,1,1,0},
		  {0,1,1,1,0,0,0,0,1,1,1,0},
		  {1,1,1,0,0,0,0,0,1,1,1,0},
		  {1,1,0,0,0,0,0,0,1,1,1,0},
		  {0,0,0,0,0,0,0,1,1,1,0,0},
		  {0,0,0,0,0,0,1,1,1,0,0,0},
		  {0,0,0,0,0,1,1,1,0,0,0,0},
		  {0,0,0,0,1,1,1,0,0,0,0,0},
		  {0,0,0,1,1,1,0,0,0,0,0,0},
		  {0,0,1,1,1,0,0,0,0,0,0,0},
		  {0,1,1,1,0,0,0,0,0,0,0,0},
		  {1,1,1,1,1,1,1,1,1,1,1,1},
		  {1,1,1,1,1,1,1,1,1,1,1,1},
		  {1,1,1,1,1,1,1,1,1,1,1,1},
};

uint8_t test_3[16][12]={
		  {0,0,1,1,1,1,1,1,1,0,0,0},
		  {0,1,1,1,1,1,1,1,1,1,0,0},
		  {1,1,1,1,1,1,1,1,1,1,1,0},
		  {1,1,1,0,0,0,0,1,1,1,1,0},
		  {0,0,0,0,0,0,0,0,1,1,1,0},
		  {0,0,0,0,0,0,0,0,1,1,1,0},
		  {0,0,0,0,0,0,0,1,1,1,0,0},
		  {0,0,0,0,0,1,1,1,1,0,0,0},
		  {0,0,0,0,0,1,1,1,1,0,0,0},
		  {0,0,0,0,0,0,0,1,1,1,0,0},
		  {0,0,0,0,0,0,0,0,1,1,1,0},
		  {0,0,0,0,0,0,0,0,1,1,1,0},
		  {1,1,1,0,0,0,0,1,1,1,1,0},
		  {1,1,1,1,1,1,1,1,1,1,1,0},
		  {0,1,1,1,1,1,1,1,1,1,0,0},
		  {0,0,1,1,1,1,1,1,1,0,0,0},
};

uint8_t test_4[16][12]={
		  {0,0,0,0,0,0,0,1,1,1,0,0},
		  {0,0,0,0,0,0,1,1,1,1,0,0},
		  {0,0,0,0,0,1,1,1,1,1,0,0},
		  {0,0,0,0,1,1,1,1,1,1,0,0},
		  {0,0,0,1,1,1,0,1,1,1,0,0},
		  {0,0,1,1,1,0,0,1,1,1,0,0},
		  {0,1,1,1,0,0,0,1,1,1,0,0},
		  {1,1,1,0,0,0,0,1,1,1,0,0},
		  {1,1,1,1,1,1,1,1,1,1,1,1},
		  {1,1,1,1,1,1,1,1,1,1,1,1},
		  {1,1,1,1,1,1,1,1,1,1,1,1},
		  {0,0,0,0,0,0,0,1,1,1,0,0},
		  {0,0,0,0,0,0,0,1,1,1,0,0},
		  {0,0,0,0,0,0,0,1,1,1,0,0},
		  {0,0,0,0,0,0,0,1,1,1,0,0},
		  {0,0,0,0,0,0,0,1,1,1,0,0},
};

uint8_t test_5[16][12]={
		  {0,0,1,1,1,1,1,1,1,1,1,0},
		  {0,0,1,1,1,1,1,1,1,1,1,0},
		  {0,0,1,1,1,1,1,1,1,1,1,0},
		  {0,0,1,1,1,0,0,0,0,0,0,0},
		  {0,0,1,1,1,0,0,0,0,0,0,0},
		  {0,0,1,1,1,1,1,1,0,0,0,0},
		  {0,0,1,1,1,1,1,1,1,1,0,0},
		  {0,0,1,1,1,1,1,1,1,1,1,0},
		  {0,0,0,0,0,0,0,0,1,1,1,0},
		  {0,0,0,0,0,0,0,0,0,1,1,1},
		  {0,0,0,0,0,0,0,0,0,1,1,1},
		  {0,0,0,0,0,0,0,0,0,1,1,1},
		  {0,0,0,0,0,0,0,0,1,1,1,0},
		  {0,0,1,1,1,1,1,1,1,1,1,0},
		  {0,0,1,1,1,1,1,1,1,1,0,0},
		  {0,0,1,1,1,1,1,1,1,0,0,0},
};

uint8_t test_6[16][12]={
		  {0,0,0,0,1,1,1,1,1,1,1,1},
		  {0,0,1,1,1,1,1,1,1,1,1,1},
		  {0,1,1,1,1,1,1,1,1,1,1,1},
		  {0,1,1,1,0,0,0,0,0,0,0,0},
		  {1,1,1,0,0,0,0,0,0,0,0,0},
		  {1,1,1,0,0,1,1,1,1,0,0,0},
		  {1,1,1,1,1,1,1,1,1,1,0,0},
		  {1,1,1,1,1,1,1,1,1,1,1,0},
		  {1,1,1,1,0,0,0,0,1,1,1,0},
		  {1,1,1,0,0,0,0,0,0,1,1,1},
		  {1,1,1,0,0,0,0,0,0,1,1,1},
		  {1,1,1,0,0,0,0,0,0,1,1,1},
		  {1,1,1,1,0,0,0,0,1,1,1,0},
		  {1,1,1,1,1,1,1,1,1,1,1,0},
		  {0,1,1,1,1,1,1,1,1,1,0,0},
		  {0,0,1,1,1,1,1,1,1,0,0,0},
};

uint8_t test_7[16][12]={
		  {1,1,1,1,1,1,1,1,1,1,1,1},
		  {1,1,1,1,1,1,1,1,1,1,1,1},
		  {1,1,1,1,1,1,1,1,1,1,1,1},
		  {0,0,0,0,0,0,0,0,1,1,1,0},
		  {0,0,0,0,0,0,0,0,1,1,1,0},
		  {0,0,0,0,0,0,0,1,1,1,0,0},
		  {0,0,0,0,0,0,0,1,1,1,0,0},
		  {0,0,0,0,0,0,1,1,1,0,0,0},
		  {0,0,0,0,0,0,1,1,1,0,0,0},
		  {0,0,0,0,0,1,1,1,0,0,0,0},
		  {0,0,0,0,0,1,1,1,0,0,0,0},
		  {0,0,0,0,1,1,1,0,0,0,0,0},
		  {0,0,0,0,1,1,1,0,0,0,0,0},
		  {0,0,0,1,1,1,0,0,0,0,0,0},
		  {0,0,1,1,1,0,0,0,0,0,0,0},
		  {0,0,1,1,1,0,0,0,0,0,0,0},
};

uint8_t test_8[16][12]={
		  {0,0,1,1,1,1,1,1,1,1,0,0},
		  {0,1,1,1,1,1,1,1,1,1,1,0},
		  {1,1,1,1,0,0,0,0,1,1,1,1},
		  {1,1,1,0,0,0,0,0,0,1,1,1},
		  {1,1,1,0,0,0,0,0,0,1,1,1},
		  {1,1,1,0,0,0,0,0,0,1,1,1},
		  {0,1,1,1,0,0,0,0,1,1,1,0},
		  {0,0,1,1,1,1,1,1,1,1,0,0},
		  {0,0,1,1,1,1,1,1,1,1,0,0},
		  {0,1,1,1,0,0,0,0,1,1,1,0},
		  {1,1,1,0,0,0,0,0,0,1,1,1},
		  {1,1,1,0,0,0,0,0,0,1,1,1},
		  {1,1,1,0,0,0,0,0,0,1,1,1},
		  {1,1,1,1,0,0,0,0,1,1,1,1},
		  {0,1,1,1,1,1,1,1,1,1,1,0},
		  {0,0,1,1,1,1,1,1,1,1,0,0},
};

uint8_t test_9[16][12]={
		  {0,0,0,0,1,1,1,1,1,1,0,0},
		  {0,0,1,1,1,1,1,1,1,1,1,0},
		  {0,1,1,1,1,1,1,1,1,1,1,1},
		  {0,1,1,1,1,0,0,0,1,1,1,1},
		  {0,1,1,1,0,0,0,0,0,1,1,1},
		  {0,1,1,1,0,0,0,0,0,1,1,1},
		  {0,1,1,1,1,0,0,0,1,1,1,1},
		  {0,1,1,1,1,1,1,1,1,1,1,1},
		  {0,0,1,1,1,1,1,1,1,1,1,1},
		  {0,0,0,1,1,1,1,1,0,1,1,1},
		  {0,0,0,0,0,0,0,0,0,1,1,1},
		  {0,0,0,0,0,0,0,0,0,1,1,1},
		  {0,0,0,0,0,0,0,0,1,1,1,0},
		  {0,1,1,1,1,1,1,1,1,1,1,0},
		  {0,1,1,1,1,1,1,1,1,1,0,0},
		  {0,1,1,1,1,1,1,1,0,0,0,0},
};

int main(void)
{
  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration----------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_DMA_Init();
  MX_USART3_UART_Init();
  MX_TIM1_Init();
  HAL_TIM_Encoder_Start(&htim1, TIM_CHANNEL_ALL);
  /* USER CODE BEGIN 2 */

  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  int capture = 0;
  char buf[25];

//  int Y = 10;
  int X = 0;
  int PANEL_X_MAX=128;


  memcpy(command.red_bin_u, red_bin, sizeof(command.red_bin_u));
  memcpy(command.green_bin_u, green_bin, sizeof(command.green_bin_u));
  memcpy(command.blue_bin_u, blue_bin, sizeof(command.blue_bin_u));

  while (1)
  {
	  capture = TIM1->CNT;
	  X = round(capture/100);
//	  sprintf(buf,"count: [%ld]\n", sizeof(red_bin));
//	  HAL_UART_Transmit_DMA(&huart3,buf,sizeof(buf)-1);

	  HAL_UART_Receive_DMA(&huart3, (int *)&command, sizeof(red_bin)*3-1);


	  memcpy(red_bin, command.red_bin_u, sizeof(red_bin));
	  memcpy(green_bin, command.green_bin_u, sizeof(green_bin));
	  memcpy(blue_bin, command.blue_bin_u, sizeof(blue_bin));

	  uint8_t pixel[32][128];
	  for (int j = 0; j < 32; j++)
		  {
		  	  for (int i = 0; i < PANEL_X_MAX; i++)
		 		  {
		  		  	  pixel[j][i]=0;
		 		  }
		  }


//	  for (int j = 0; j < 16; j++)
//		  {
//		  	  for (int i = 0; i < 12; i++)
//		 		  {
//		  		  	  pixel[j+8][X + i]=test_9[j][i];
//		 		  }
//		  }

	  uint8_t pixel_2[16][26];

	  for (int j = 0; j < 16; j++)
		  {
		  	  for (int i = 0; i < 26; i++)
		 		  {
		  		pixel_2[j][i]=0;
		 		  }
		  }

	  uint8_t pixel_1[16][12];
	  int numbers[2];
	  int test_data = 59;
	  int tens = test_data/10;
	  int ones = test_data%10;
	  numbers[0]=tens;
	  numbers[1]=ones;

	  for (int pix = 0; pix < 2; pix++){
		  for (int j = 0; j < 16; j++)
			  {
				  for (int i = 0; i < 12; i++)
					  {
					  	  switch(numbers[pix]){
					  	  case 0:
//					  		pixel_1 = test_0;
					  		memcpy(pixel_1, test_0, sizeof(pixel_1));
					  		break;
					  	  case 1:
					  		memcpy(pixel_1, test_1, sizeof(pixel_1));
					  		break;
					  	  case 2:
					  		memcpy(pixel_1, test_2, sizeof(pixel_1));
					  		break;
					  	  case 3:
					  		memcpy(pixel_1, test_3, sizeof(pixel_1));
					  		break;
					  	  case 4:
					  		memcpy(pixel_1, test_4, sizeof(pixel_1));
					  		break;
					  	  case 5:
					  		memcpy(pixel_1, test_5, sizeof(pixel_1));
					  		break;
					  	  case 6:
					  		memcpy(pixel_1, test_6, sizeof(pixel_1));
					  		break;
					  	  case 7:
					  		memcpy(pixel_1, test_7, sizeof(pixel_1));
					  		break;
					  	  case 8:
					  		memcpy(pixel_1, test_8, sizeof(pixel_1));
					  		break;
					  	  case 9:
					  		memcpy(pixel_1, test_9, sizeof(pixel_1));
					  		break;
					  	  }
//						  pixel[j+8][X + i]=test_9[j][i];
					  	pixel_2[j][pix*12 + pix*2 + i]=pixel_1[j][i];
					  }
			  }
	  }

	  for (int j = 0; j < 16; j++)
		  {
			  for (int i = 0; i < 26; i++)
				  {
					  pixel[j+8][X + i]=pixel_2[j][i];
				  }
		  }






	  int Y = 0;

	  for(int z = 0; z<3; z++){
	  for( int j = 0; j<16;j++){
//		  HAL_Delay(1);
//		  for(int d=0;d<0x500;d++);
//		  HAL_GPIO_WritePin(GPIOA,GPIO_PIN_5,GPIO_PIN_SET);

		  Y= j;

		  set_row(Y);
//		  Y++;
		  int x = 0;
//		  for (int i = 0; i < PANEL_X_MAX; i++)
//		  {
//			  //top
//			  HAL_GPIO_WritePin(GPIOB,GPIO_PIN_5,pixel[Y][i]);
//			  //bottom
////			  HAL_GPIO_WritePin(GPIOB,GPIO_PIN_8,pixel[Y+16][i]);
//			  HAL_GPIO_WritePin(GPIOB,GPIO_PIN_6,pixel[Y+16][i]);
//			  HAL_GPIO_WritePin(GPIOB,GPIO_PIN_7,pixel[Y+16][i]);
//
//			  clock();
//		  } // for X


		  uint64_t one = 1;
		  for (int i = 0; i < 64; i++)
		  {
			  //top
			  HAL_GPIO_WritePin(GPIOB,GPIO_PIN_3,(green_bin[z][Y]>>(63-i)&one));
			  HAL_GPIO_WritePin(GPIOB,GPIO_PIN_4,(red_bin[z][Y]>>(63-i)&one));
			  HAL_GPIO_WritePin(GPIOB,GPIO_PIN_5,(blue_bin[z][Y]>>(63-i)&one));
			  //bottom
			  HAL_GPIO_WritePin(GPIOB,GPIO_PIN_6,(green_bin[z][Y+16]>>(63-i)&one));
			  HAL_GPIO_WritePin(GPIOB,GPIO_PIN_7,(red_bin[z][Y+16]>>(63-i)&one));
			  HAL_GPIO_WritePin(GPIOB,GPIO_PIN_8,(blue_bin[z][Y+16]>>(63-i)&one));
			  clock();
		  } // for X
		  for (int i = 0; i < 64; i++)
		  {
			  //top
			  HAL_GPIO_WritePin(GPIOB,GPIO_PIN_3,(green_bin[z][Y+32]>>(63-i)&one));
			  HAL_GPIO_WritePin(GPIOB,GPIO_PIN_4,(red_bin[z][Y+32]>>(63-i)&one));
			  HAL_GPIO_WritePin(GPIOB,GPIO_PIN_5,(blue_bin[z][Y+32]>>(63-i)&one));
			  //bottom
			  HAL_GPIO_WritePin(GPIOB,GPIO_PIN_6,(green_bin[z][Y+48]>>(63-i)&one));
			  HAL_GPIO_WritePin(GPIOB,GPIO_PIN_7,(red_bin[z][Y+48]>>(63-i)&one));
			  HAL_GPIO_WritePin(GPIOB,GPIO_PIN_8,(blue_bin[z][Y+48]>>(63-i)&one));
			  clock();
		  } // for X
		  lat();
		  oe();
//		  HAL_GPIO_WritePin(GPIOA,GPIO_PIN_5,GPIO_PIN_RESET);
	  } // for Y
	  }//z


	} //while

  } //main

void clock(){
	  HAL_GPIO_WritePin(GPIOA,GPIO_PIN_4,GPIO_PIN_RESET);
	  HAL_GPIO_WritePin(GPIOA,GPIO_PIN_4,GPIO_PIN_SET);
}

void lat(){
	  HAL_GPIO_WritePin(GPIOA,GPIO_PIN_6,GPIO_PIN_RESET);
	  HAL_GPIO_WritePin(GPIOA,GPIO_PIN_6,GPIO_PIN_SET);
	  HAL_GPIO_WritePin(GPIOA,GPIO_PIN_6,GPIO_PIN_RESET);
}
void oe(){
	  HAL_GPIO_WritePin(GPIOA,GPIO_PIN_5,GPIO_PIN_RESET);
	  for(int d=0;d<0x50;d++);
	  HAL_GPIO_WritePin(GPIOA,GPIO_PIN_5,GPIO_PIN_SET);
}

void set_row(int row){
	  if (row & 0x01) HAL_GPIO_WritePin(GPIOA,GPIO_PIN_0,GPIO_PIN_SET);
	  else HAL_GPIO_WritePin(GPIOA,GPIO_PIN_0,GPIO_PIN_RESET);

	  if (row & 0x02) HAL_GPIO_WritePin(GPIOA,GPIO_PIN_1,GPIO_PIN_SET);
	  else HAL_GPIO_WritePin(GPIOA,GPIO_PIN_1,GPIO_PIN_RESET);

	  if (row & 0x04) HAL_GPIO_WritePin(GPIOA,GPIO_PIN_2,GPIO_PIN_SET);
	  else HAL_GPIO_WritePin(GPIOA,GPIO_PIN_2,GPIO_PIN_RESET);

	  if (row & 0x08) HAL_GPIO_WritePin(GPIOA,GPIO_PIN_3,GPIO_PIN_SET);
	  else HAL_GPIO_WritePin(GPIOA,GPIO_PIN_3,GPIO_PIN_RESET);
}


/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{

  RCC_OscInitTypeDef RCC_OscInitStruct;
  RCC_ClkInitTypeDef RCC_ClkInitStruct;

    /**Initializes the CPU, AHB and APB busses clocks
    */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE;
  RCC_OscInitStruct.HSEState = RCC_HSE_ON;
  RCC_OscInitStruct.HSEPredivValue = RCC_HSE_PREDIV_DIV1;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
  RCC_OscInitStruct.PLL.PLLMUL = RCC_PLL_MUL9;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

    /**Initializes the CPU, AHB and APB busses clocks
    */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV2;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_2) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

    /**Configure the Systick interrupt time
    */
  HAL_SYSTICK_Config(HAL_RCC_GetHCLKFreq()/1000);

    /**Configure the Systick
    */
  HAL_SYSTICK_CLKSourceConfig(SYSTICK_CLKSOURCE_HCLK);

  /* SysTick_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(SysTick_IRQn, 0, 0);
}

/* TIM1 init function */
static void MX_TIM1_Init(void)
{

  TIM_Encoder_InitTypeDef sConfig;
  TIM_MasterConfigTypeDef sMasterConfig;

  htim1.Instance = TIM1;
  htim1.Init.Prescaler = 0;
  htim1.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim1.Init.Period = 12800;
  htim1.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim1.Init.RepetitionCounter = 0;
  htim1.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  sConfig.EncoderMode = TIM_ENCODERMODE_TI12;
  sConfig.IC1Polarity = TIM_ICPOLARITY_RISING;
  sConfig.IC1Selection = TIM_ICSELECTION_DIRECTTI;
  sConfig.IC1Prescaler = TIM_ICPSC_DIV1;
  sConfig.IC1Filter = 0;
  sConfig.IC2Polarity = TIM_ICPOLARITY_RISING;
  sConfig.IC2Selection = TIM_ICSELECTION_DIRECTTI;
  sConfig.IC2Prescaler = TIM_ICPSC_DIV1;
  sConfig.IC2Filter = 0;
  if (HAL_TIM_Encoder_Init(&htim1, &sConfig) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim1, &sMasterConfig) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

}

/* USART3 init function */
static void MX_USART3_UART_Init(void)
{

  huart3.Instance = USART3;
  huart3.Init.BaudRate = 57600;
  huart3.Init.WordLength = UART_WORDLENGTH_8B;
  huart3.Init.StopBits = UART_STOPBITS_1;
  huart3.Init.Parity = UART_PARITY_NONE;
  huart3.Init.Mode = UART_MODE_TX_RX;
  huart3.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart3.Init.OverSampling = UART_OVERSAMPLING_16;
  if (HAL_UART_Init(&huart3) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

}

/**
  * Enable DMA controller clock
  */
static void MX_DMA_Init(void)
{
  /* DMA controller clock enable */
  __HAL_RCC_DMA1_CLK_ENABLE();

  /* DMA interrupt init */
  /* DMA1_Channel2_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(DMA1_Channel2_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(DMA1_Channel2_IRQn);
  /* DMA1_Channel3_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(DMA1_Channel3_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(DMA1_Channel3_IRQn);

}

/** Configure pins as
        * Analog
        * Input
        * Output
        * EVENT_OUT
        * EXTI
*/
static void MX_GPIO_Init(void)
{

  GPIO_InitTypeDef GPIO_InitStruct;

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOC_CLK_ENABLE();
  __HAL_RCC_GPIOD_CLK_ENABLE();
  __HAL_RCC_GPIOA_CLK_ENABLE();
  __HAL_RCC_GPIOB_CLK_ENABLE();

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOC, GPIO_PIN_13, GPIO_PIN_SET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOA, GPIO_PIN_0|GPIO_PIN_1|GPIO_PIN_2|GPIO_PIN_3
                          |GPIO_PIN_4|GPIO_PIN_5|GPIO_PIN_6, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOB, GPIO_PIN_3|GPIO_PIN_4|GPIO_PIN_5|GPIO_PIN_6
                          |GPIO_PIN_7|GPIO_PIN_8, GPIO_PIN_RESET);

  /*Configure GPIO pin : PC13 */
  GPIO_InitStruct.Pin = GPIO_PIN_13;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);

  /*Configure GPIO pins : PA0 PA1 PA2 PA3
                           PA4 PA5 PA6 */
  GPIO_InitStruct.Pin = GPIO_PIN_0|GPIO_PIN_1|GPIO_PIN_2|GPIO_PIN_3
                          |GPIO_PIN_4|GPIO_PIN_5|GPIO_PIN_6;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

  /*Configure GPIO pins : PB3 PB4 PB5 PB6
                           PB7 PB8 */
  GPIO_InitStruct.Pin = GPIO_PIN_3|GPIO_PIN_4|GPIO_PIN_5|GPIO_PIN_6
                          |GPIO_PIN_7|GPIO_PIN_8;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

}

/* USER CODE BEGIN 4 */

/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @param  file: The file name as string.
  * @param  line: The line in file as a number.
  * @retval None
  */
void _Error_Handler(char *file, int line)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
  while(1)
  {
  }
  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t* file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     tex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/**
  * @}
  */

/**
  * @}
  */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
